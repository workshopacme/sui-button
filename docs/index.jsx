import React from 'react';
import {Button} from '../src';
import '../src/index.scss';

React.render(<Button label='Normal' rest/>, document.getElementById('normal'));
React.render(<Button label='Hover' hover/>, document.getElementById('hover'));
React.render(<Button label='Active' active/>, document.getElementById('active'));
React.render(<Button label='Disabled' disabled/>, document.getElementById('disabled'));

React.render(<Button label='Normal' rest/>, document.getElementById('normal-normal'));
React.render(<Button label='Normal Large' rest large/>, document.getElementById('normal-large'));
React.render(<Button label='Normal Extra' rest extraLarge handleClick={()=>alert()}/>, document.getElementById('normal-extra-large'));

React.render(<Button label='User icon' icon='user' rest/>, document.getElementById('normal-user'));
React.render(<Button label='Clock icon' icon='clock-o' rest large/>, document.getElementById('normal-clock'));
React.render(<Button label='Edit icon' icon='pencil-square-o' rest extraLarge handleClick={()=>alert()}/>, document.getElementById('normal-edit'));
