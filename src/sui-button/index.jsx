import React from 'react';
import cx from 'classnames';

export default class Button extends React.Component {
  render() {
    const classes = cx({
      'sui-Button': true,
      'is-rest': this.props.rest,
      'is-hover': this.props.hover,
      'is-active': this.props.active,
      'is-disabled': this.props.disabled,
      'sui-Button--large': this.props.large,
      'sui-Button--extraLarge': this.props.extraLarge
    });
    const iconClass = cx({
      'sui-Button-icon': true,
      'fa': true,
      ['fa-' + this.props.icon]: true
    });
    const icon = this.props.icon ? <i className={iconClass}></i> : '';
    return (
      <button className={classes} onClick={this.props.handleClick}>
        {icon}
        {this.props.label}
      </button>
    );
  }
}

Button.propTypes = {
  handleClick: React.PropTypes.func,
  label: React.PropTypes.string.required,
  rest: React.PropTypes.bool,
  hover: React.PropTypes.bool,
  active: React.PropTypes.bool,
  disabled: React.PropTypes.bool,
  large: React.PropTypes.bool,
  extraLarge: React.PropTypes.bool,
  icon: React.PropTypes.string
};
