var assert = require("assert");
var React = require('react');
var TestUtils = require('react/lib/ReactTestUtils');
var expect = require('expect');
var Button = require('../src/sui-button');

describe('sui-button component test suite', function () {
  it('loads without problems', function () {
    assert.notEqual(undefined, Button);
  });

  it('renders into document', function() {
    var root = TestUtils.renderIntoDocument(<Button />);
    expect(root).toExist();
  });
});
